English:

This drivers are for TBS6981, TBS6980, TBS6984, BGT36xx DVB-S/S2 and DVB-T/T2 cards.
These drivers are built against the latest media tree and I use both tbs6981 and BGT3600 cards together without any problems.
This drivers are distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

Português:

Estes drivers são para as placas TBS6981, TBS6980, TBS6984, BGT36xx DVB-S/S2 e DVB-T/T2.
São utilizadas as mais recentes versões media e eu uso tanto a tbs6981 como a BGT3600 juntas sem problemas.
Estes drivers são distribuídos na esperança que sejam úteis, mas SEM QUALQUER GARANTIA, nem mesmo a garantia implícita de COMERCIALIZAÇÃO ou ADEQUAÇÃO A UM DETERMINADO FIM.

Installation/Instalação

$ mkdir build

$ cd build

$ git clone git://linuxtv.org/media_build.git

$ git clone https://bitbucket.org/jamlazevedo/tbs_bgt_linux.git

$cd  media_build

$ make dir DIR=../tbs_bgt_linux

$ make distclean

$ make

$ sudo make install

$ sudo reboot
